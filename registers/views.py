import json
from purchasesAPI.mixins import JsonResponseMixin
from django.core.serializers import serialize
from django.views.generic import View
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render

from .models import Register, RegisterQuerySet, RegisterManager

obj = Register.objects.all()

class SerilaizeDetailView(View):
    def get(self, request, *args, **kwargs):
        obj = Register.objects.get(id=2) 
        json_data = obj.serialize()
        return HttpResponse(json_data, content_type='application/json')

class SerilaizeListView(View):
    def get(self, request, *args, **kwargs):
        qs = Register.objects.all()                                     
        json_data = Register.objects.all()                              
        return HttpResponse(json_data, content_type='application/json')        
                    